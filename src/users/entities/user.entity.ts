export class User {
  id: number;
  login: string;
  password: string;
  roles: ('admin' | 'users')[];
  gender: 'male' | 'female';
  age: number;
}
